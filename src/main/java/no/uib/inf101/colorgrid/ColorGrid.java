package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {
  int rows = 0;
  int cols = 0;
  List<List<CellColor>> grid;


  public ColorGrid(int rows, int cols){
    this.rows = rows;
    this.cols = cols;
    grid = new ArrayList<>();
    for (int i = 0; i < rows; i++){
      List<CellColor> rowlist = new ArrayList<CellColor>();
      for (int j = 0; j < cols; j++) {
        rowlist.add(new CellColor(new CellPosition(i, j), null));
      }
      grid.add(rowlist);
    }
  }

  @Override
  public int rows() {
    return rows;
  }

  @Override
  public int cols() {
    return cols;
  }
  
  @Override
  public List<CellColor> getCells() {
    List<CellColor> cells = new ArrayList<CellColor>();
    for (int i = 0; i < this.rows; i++){
      for (int j = 0; j < this.cols; j++){
        cells.add(this.grid.get(i).get(j));
      }
    }
    return cells;
  }

  @Override
  public void set(CellPosition pos, Color color) {
    int row = pos.row();
    int col = pos.col();
    CellColor cellColor = new CellColor(pos, color);
    List<CellColor> thisRow = grid.get(row);
    thisRow.set(col, cellColor);
  }
  @Override
  public Color get(CellPosition pos) {
    List<CellColor> row = grid.get(pos.row());
    return row.get(pos.col()).color(); 
  }

}
    
   
  


