package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

import java.awt.geom.Rectangle2D;

public class CellPositionToPixelConverter {

  // The field variable in the class
  Rectangle2D box;
  GridDimension gd;
  Double margin;

  // Constructor - Here we set the values of the field variables
  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) { //Konstruktøren
    this.box = box; 
    this.gd = gd;
    this.margin = margin;
  
  }

  public Rectangle2D getBoundsForCell(CellPosition CellPosition) {
    double width = box.getWidth();
    double height = box.getHeight();
    double rows = gd.rows();
    double cols = gd.cols();

    double cellWidth = (width - this.margin * cols - this.margin) / cols;
    //(width - ((this.margin * (cols + 1))) / cols); // finner vellevidden fra bredden til bakgrunden
    double cellHeight = (height - this.margin * rows - this.margin) / rows ;
    //(height - ((this.margin * (rows + 1))) / rows);

    double cellX = box.getX() + this.margin + (cellWidth + this.margin) * CellPosition.col();
    //box.getX() + (this.margin * (CellPosition.col() + 1)) + (cellWidth * CellPosition.col()); // finner x og y verdi
    double cellY = box.getY() + this.margin + (cellHeight + this.margin) * CellPosition.row();
    //box.getY() + (this.margin * (CellPosition.row() + 1)) + (cellHeight * CellPosition.row());

    Rectangle2D rectangle = new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight); 
    return rectangle;
  }
}
