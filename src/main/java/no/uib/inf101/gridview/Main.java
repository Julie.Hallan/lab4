package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;
import javax.swing.JFrame;
import java.awt.Color;
import java.util.Set;

public class Main {
  public static void main(String[] args) {
    // Opprett et rutenett med 3 rader og 4 kolonner
    IColorGrid grid = new ColorGrid(3, 4);
    JFrame frame = new JFrame();
    
    grid.set(new CellPosition(0, 0), Color.RED);
    grid.set(new CellPosition(0, 3), Color.BLUE);
    grid.set(new CellPosition(2, 0), Color.YELLOW);
    grid.set(new CellPosition(2, 3), Color.GREEN);
    GridView view = new GridView(grid);
    
    frame.setTitle("INF101");
    frame.setContentPane(view);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
    
  }
}
